public class Config {
    public static final Double EVAPORATION_FACTOR = 0.1;
    public static final Double MIN_PHEROMONE = Math.pow(10, -10);
    public static final Double ALPHA = 1.0;
    public static final Double BETA = 5.0;
    public static final Integer ITERATIONS = 1000;
    public static final Integer MAX_CORRECTIONS = 100;
    public static final Integer RUNS = 10;

    public static final String FILE_NAME = "instances2/1Cap10.txt";
    public static final String DELIMITER = " ";
}
