import java.util.ArrayList;

public class Solution {

    private Double cost;

    private static Solution best = null;
    private static Solution totalBest = null;

    private final ArrayList<Link> links = new ArrayList<>();

    private ArrayList<Warehouse> warehouses = new ArrayList<>();

    public Solution(ArrayList<Link> links) {
        this.links.addAll(links);

        evaluateCost();

        if(best == null || best.cost > cost)
            best = this;

        if(totalBest == null || totalBest.cost > cost)
            totalBest = this;
    }

    private Double evaluateCost(){
        Double sum = 0.0;

        warehouses.clear();

        for (Link l:links) {
            sum += l.getTransportCost();
            Warehouse w = l.getWarehouse();

            if(!warehouses.contains(w))
            {
                warehouses.add(w);
                sum += w.getCost();
            }
        }

        cost = sum;
        return sum;
    }

    public static Solution getBest() {
        return best;
    }

    public static Solution getTotalBest() {
        return totalBest;
    }

    public static ArrayList<Link> getBestLinks() {
        return best.links;
    }

    public static void resetBest(){
        best = null;
    }

    @Override
    public String toString() {
        return "Solution{" +
                "cost=" + cost +
                ", warehouses.size()=" + warehouses.size() +
                ", warehouses=" + warehouses +
                '}';
    }

    public static Solution localSearch(Solution sol){
        Boolean searching = true;
        Solution correctedSol = sol;
        int i = 0;

        while(searching){
            if(i > Config.MAX_CORRECTIONS)
                break;

            correctedSol = correctSolution(sol);
            if(correctedSol.cost < sol.cost)
                sol = correctedSol;
            else
                searching = false;
            i++;
        }

        return correctedSol;
    }

    public static Solution correctSolution(Solution sol) {
        Warehouse.resetLinks(sol.links);
        Solution bestCorrected = sol;

        for (Solution s:sol.getNeighbouringSolutions())
            if(s.cost < sol.cost)
                bestCorrected = s;

        return bestCorrected;
    }

    public ArrayList<Solution> getNeighbouringSolutions(){
        ArrayList<Solution> solutions = new ArrayList<>();

        for (Link l:links)
            solutions.addAll(getNeighbouringSolutionsForLink(l));

        return solutions;
    }

    public ArrayList<Solution> getNeighbouringSolutionsForLink(Link link){
        ArrayList<Solution> receivedSolutions = new ArrayList<>();

        ArrayList<Link> availableLinks = new ArrayList<>(link.getCustomer().getLinks());
        availableLinks.removeIf(l -> !l.getWarehouse().canSupply(l));
        availableLinks.remove(link);

        ArrayList<Link> solutionLinks = new ArrayList<>(links);
        solutionLinks.remove(link);

        for (Link l:availableLinks) {
            ArrayList<Link> finalSolutionLinks = new ArrayList<>(solutionLinks);
            finalSolutionLinks.add(l);

            receivedSolutions.add(new Solution(finalSolutionLinks));
        }

        return receivedSolutions;
    }
}
