import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class ACO {
    public static boolean loadLine(Scanner scanner){
        try {
            ArrayList<String> line = new ArrayList<>();

            String lineString = scanner.nextLine();
            Collections.addAll(line, lineString.split(Config.DELIMITER));
            line.removeIf(item -> item == null || "".equals(item));

            Warehouse w = Warehouse.getWarehouseById(Integer.parseInt(line.get(0)));
            Customer c = Customer.getCustomerById(Integer.parseInt(line.get(1)));
            Double cost = Double.parseDouble(line.get(2));
            Double demand = Double.parseDouble(line.get(3));

            Link l = new Link(c, w, cost, demand);
            c.addLink(l);
        }
        catch (IndexOutOfBoundsException e){
            return false;
        }
        return true;
    }

    public static void loadData(String path) {
        Warehouse.clearWarehouses();
        Customer.clearCustomers();
        Link.clearLinks();

        try {
            Scanner scanner = new Scanner(new File(path));

            for(int i = 0; i < 5; i++)
                scanner.nextLine();

            Boolean loading = true;

            while(loading)
                loading = loadLine(scanner);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static Solution ACO(String fileName){
        loadData(fileName);
        Link.initPheromones();

        Solution.resetBest();

        for(int i = 0; i < Config.ITERATIONS; i++){
            Customer.assignLinks();

            ArrayList<Link> links = Customer.getChosenLinks();

            Solution sol = new Solution(links);
            Solution.localSearch(sol);

            Link.updatePheromone();
        }

        return Solution.getBest();
    }

    public static void main(String[] args) {
        for (int i = 1; i <= 20; i++)
            System.out.println(i + ". " + ACO("instances2/" + i + "Cap10.txt"));
    }
}
