import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Customer{

    public static final ArrayList<Customer> customers = new ArrayList<>();
    private static final Random rng = new Random();

    private Integer id;
    private Link link;


    private final ArrayList<Link> links = new ArrayList<>();

    public Customer(Integer id) {
        super();
        this.id = id;
        customers.add(this);
    }

    public static Customer getCustomerById(Integer id){
        for (Customer c:customers)
            if(c.id == id)
                return c;

        return new Customer(id);
    }

    public void assign(Link link) {
        this.link = link;
    }

    public ArrayList<Link> getLinks() {
        return links;
    }

    private void setupAttractiveness(){
        for (Link l:links)
            l.calcAttractiveness();
    }

    private void setupProbabilities(){
        Double sum = 0.0;

        for (Link l:links)
            sum += l.getBias();

        for (Link l:links)
            l.calcProbability(sum);
    }

    private Link getRandomLink(){
        Double val = rng.nextDouble();
        Double sum = 0.0;

        for (Link l:links) {
            if(val < sum + l.getProbability())
                return l;
            else
                sum += l.getProbability();
        }

        System.out.println(this + " link jest nullem!");
        System.out.println(sum);

        for (Link l:links)
            System.out.println(l);

        return null;
    }

    private void assignLink(){
        setupAttractiveness();
        setupProbabilities();
        getRandomLink().assign();
    }

    public static void assignLinks(){
        Warehouse.clearAssignments();

        ArrayList<Customer> shuffledCustomers = new ArrayList<>(customers);
        Collections.shuffle(shuffledCustomers);

        for (Customer c:shuffledCustomers)
            c.assignLink();
    }

    public static ArrayList<Link> getChosenLinks(){
        ArrayList<Link> links = new ArrayList<>();

        for (Customer c:customers)
            links.add(c.link);

        return links;
    }

    public static void clearCustomers(){
        customers.clear();
    }

    public void addLink(Link l){
        links.add(l);
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                '}';
    }
}
