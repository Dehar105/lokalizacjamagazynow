import java.util.ArrayList;
import java.util.HashSet;

public class Warehouse {

    private Integer id;
    private Double capacity = 10.0;
    private Double cost = 100.0;

    public static final ArrayList<Warehouse> warehouses = new ArrayList<>();

    private final HashSet<Link> links = new HashSet<>();

    public Warehouse(Integer id) {
        super();
        this.id = id;

        warehouses.add(this);
    }

    public static Warehouse getWarehouseById(Integer id){
        for (Warehouse w:warehouses)
            if(w.id == id)
                return w;

        return new Warehouse(id);
    }

    public Double getCost() {
        return cost;
    }

    public Boolean isOpened() {
        return links.size() > 0;
    }

    public static ArrayList<Warehouse> getWarehousesList() {
        return warehouses;
    }

    public Double getAlreadySuppliedMaterial(){
        Double sum = 0.0;
        for (Link l:links)
            sum += l.getDemand();

        return sum;
    }

    public Boolean canSupply(Link l){
        return getAlreadySuppliedMaterial() + l.getDemand() <= capacity;
    }

    public static void clearAssignments(){
        for (Warehouse w:warehouses)
            w.links.clear();
    }

    public void assign(Link l){
        links.add(l);
    }

    public static void clearWarehouses(){
        warehouses.clear();
    }

    @Override
    public String toString() {
        return "Warehouse{" +
                "id=" + id +
                '}';
    }

    public static void resetLinks(ArrayList<Link> links){
        clearAssignments();

        for (Link l:links)
            l.getWarehouse().assign(l);
    }
}
