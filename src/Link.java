import java.util.ArrayList;

public class Link {
    private Customer customer;
    private Warehouse warehouse;
    private Double pheromone;
    private Double probability;
    private Double attractiveness;
    private Double transportCost;
    private Double demand;

    public static final ArrayList<Link> links = new ArrayList<>();

    public Link(Customer customer, Warehouse warehouse, Double transportCost, Double demand) {
        this.customer = customer;
        this.warehouse = warehouse;
        this.transportCost = transportCost;
        this.demand = demand;

        links.add(this);
    }

    public Customer getCustomer() {
        return customer;
    }

    public Double getDemand() {
        return demand;
    }

    public static void initPheromones(){
        for (Link l:links)
            l.pheromone = 1.0/(Warehouse.warehouses.size() * Customer.customers.size());
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public Double getTransportCost() {
        return transportCost;
    }

    public Double getProbability() {
        return probability;
    }

    public void calcProbability(Double biasSum) {
        if(biasSum == 0.0)
            this.probability = 0.0;
         if(Double.isInfinite(attractiveness))
             this.probability = 1.0;
         else
            this.probability = getBias() / biasSum;
    }

    public void calcAttractiveness(){
        if(warehouse.canSupply(this)){
            Double den = Config.ALPHA * transportCost;

            if(!warehouse.isOpened())
                den += Config.BETA * warehouse.getCost();

            attractiveness = 1 / den;
        }
        else
            attractiveness = 0.0;
    }

    public Double getBias(){
        return pheromone * attractiveness;
    }

    public void assign(){
        customer.assign(this);
        warehouse.assign(this);
    }

    public static void updatePheromone(){
        for (Link l:links) {
            l.pheromone *= (1 - Config.EVAPORATION_FACTOR);
            if(l.pheromone < Config.MIN_PHEROMONE)
                l.pheromone = Config.MIN_PHEROMONE;
        }

        for (Link l:Solution.getBestLinks())
            l.pheromone += Config.EVAPORATION_FACTOR/links.size();
    }

    public static void clearLinks(){
        links.clear();
    }

    @Override
    public String toString() {
        return "Link{" +
                "customer=" + customer +
                ", warehouse=" + warehouse +
                ", transportCost=" + transportCost +
                '}';
    }
}
